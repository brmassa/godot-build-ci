#!/usr/bin/env bash
#
# This build script is licensed under CC0 1.0 Universal:
# https://creativecommons.org/publicdomain/zero/1.0/

set -euo pipefail
IFS=$'\n\t'

export DIR
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
# shellcheck source=/dev/null
source "$DIR/../_common.sh"

# The target type ("release" or "release_debug")
scons_target="$1"

# Required to find pip-installed SCons
export PATH="$HOME/.local/bin:$PATH"

echo "start"

# If using PowerShell, replace `curl` with `curl.exe` below.
curl -LO https://curl.haxx.se/ca/cacert.pem
cert-sync --user cacert.pem

# Build Mono "glue"
echo "scons p=linuxbsd tools=yes module_mono_enabled=yes mono_glue=no"
scons p=linuxbsd tools=yes module_mono_enabled=yes mono_glue=no
echo "bin/godot.linuxbsd.tools.64.mono --generate-mono-glue modules/mono/glue"
bin/godot.linuxbsd.tools.64.mono --generate-mono-glue modules/mono/glue
# Build Editor
echo "scons p=linuxbsd tools=yes module_mono_enabled=yes target="$scons_target" "
scons p=linuxbsd tools=yes module_mono_enabled=yes target="$scons_target" 

# Create Linux editor ZIP archive.
cd "$GODOT_DIR/bin/"
mv "godot.linuxbsd.tools.64" "godot"
zip -r9 "$ARTIFACTS_DIR/editor/godot-linux-nightly-x86_64.zip" "godot"

make_manifest "$ARTIFACTS_DIR/editor/godot-linux-nightly-x86_64.zip"
